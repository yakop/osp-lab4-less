//
// Created by yakov on 16.11.18.
//

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include "common.h"

static void ensure_print(ssize_t count, size_t required, const char* message);

void file_open_error(const char *file_name) {
    const char *template = "Cannot open file %s";
    char *message = malloc(strlen(template) + PATH_MAX);

    if (!message) {
        die("Cannot allocate memory");
    }

    sprintf(message, template, file_name);
    die(message);
}

void die(const char *message) {
    perror(message);
    exit(1);
}

void raise(const char *message) {
    print(message);
    exit(1);
}

void print(const char *message) {
    ssize_t result;
    size_t expected = strlen(message);

    if ((result = write(STDOUT_FILENO, message, expected)) < 0) {
        die("Cannot perform write call");
    }

    ensure_print(result, expected, message);
}

void ensure_print(ssize_t count, size_t required, const char* message) {

    while ((size_t) count < required) {
        if ((count = write(STDOUT_FILENO, message += count, required -= count)) < 0) {
            die("Cannot perform write call");
        }
    }
}

size_t min(const size_t a, const size_t b) {
    return a > b ? b : a;
}
