//
// Created by yakov on 23.11.18.
//

#ifndef LESS_BUFFER_H
#define LESS_BUFFER_H

#include <stdlib.h>

#define LESS_BUFFER_SIZE 1024 * 1024 * 10 //10 MB, should be enough for any terminal

struct buffer {
    size_t current_window_start;
    size_t current_window_end;
    size_t base;
    size_t limit;
    int end_of_file;
    unsigned char *itself;
};

struct buffer *new_buffer();

int update_buffer(struct buffer *buffer, int fd);

#endif //LESS_BUFFER_H
