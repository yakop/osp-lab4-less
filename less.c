//
// Created by yakov on 16.11.18.
//

#include <stdlib.h>
#include <unistd.h>
#include <locale.h>
#include <fcntl.h>
#include <string.h>

#if defined(__FreeBSD__) || defined(__linux__)

#include <ncurses.h>

#else
#include <ncurses/ncurses.h>
#endif /* __FreeBSD__ || __linux__ */

#include "less.h"
#include "buffer.h"
#include "search.h"
#include "display.h"

#define MAX_SEARCH_PATTERN_LENGTH 100

static void reset();

static void switch_file(struct less *less, size_t number);

static ssize_t process_search_result(struct less *less, ssize_t result);

static void main_loop(struct less *less);

static size_t calculate_position(struct less *less, size_t result);

// "Constructor"
struct less *new_less(const struct options *options) {
    setlocale(LC_ALL, "");  /* Unicode will not work other way */
    initscr();              /* Start curses mode */
    cbreak();               /* cbreak io mode */
    keypad(stdscr, TRUE);   /* We need arrows */
    refresh();

    struct less *less = calloc(1, sizeof(struct less));

    if (!less) {
        die("Cannot allocate memory");
    }

    less->file_count = options->file_count;
    less->file_names = options->file_names;
    less->use_stdin = options->use_stdin;
    less->ignore_case_search = options->ignore_case_search;

    less->buffer = new_buffer(less);
    less->display = new_display(less);
    atexit(reset);

    return less;
}

void main_loop(struct less *less) {
    int next_char;
    char *search_pattern = NULL;
    int forward_search = 1;
    int command_mode = 0;

    while ((next_char = getch()) != 'q' && next_char != 'Q') {
        if (next_char == KEY_DOWN
            || strcmp(keyname(next_char), "\n") == 0
            || strcmp(keyname(next_char), "^N") == 0
            || strcmp(keyname(next_char), "^E") == 0
            || strcmp(keyname(next_char), "^J") == 0
            || strcmp(keyname(next_char), "e") == 0
            || strcmp(keyname(next_char), "j") == 0) {

            command_mode = 0;
            scroll_pad(less->display, less->buffer, DOWN, 1);
        } else if (next_char == KEY_UP
                   || strcmp(keyname(next_char), "^Y") == 0
                   || strcmp(keyname(next_char), "^P") == 0
                   || strcmp(keyname(next_char), "^K") == 0
                   || strcmp(keyname(next_char), "y") == 0
                   || strcmp(keyname(next_char), "k") == 0) {

            command_mode = 0;
            scroll_pad(less->display, less->buffer, UP, 1);
        } else if (strcmp(keyname(next_char), " ") == 0
                   || strcmp(keyname(next_char), "^V") == 0
                   || strcmp(keyname(next_char), "^F") == 0
                   || strcmp(keyname(next_char), "f") == 0) {

            command_mode = 0;
            scroll_pad(less->display, less->buffer, DOWN, (size_t) LINES);
        } else if (strcmp(keyname(next_char), "ESC-b") == 0
                   || strcmp(keyname(next_char), "^B") == 0
                   || strcmp(keyname(next_char), "b") == 0) {

            command_mode = 0;
            scroll_pad(less->display, less->buffer, UP, (size_t) LINES);
        } else if (strcmp(keyname(next_char), "p") == 0) {
            if (command_mode) {
                if (less->current_file == 0) {
                    redraw_menu(less->display, "No previous file", 1);
                } else {
                    switch_file(less, less->current_file - 1);
                    update_buffer(less->buffer, less->current_fd);
                    fill_pad(less->display, less->buffer);
                    redraw_all(less->display, ":", 0);
                }
            }
        } else if (strcmp(keyname(next_char), "n") == 0) {
            if (command_mode) {
                if (less->current_file == less->file_count - 1) {
                    redraw_menu(less->display, "No next file", 1);
                } else {
                    switch_file(less, less->current_file + 1);
                    update_buffer(less->buffer, less->current_fd);
                    fill_pad(less->display, less->buffer);
                    redraw_all(less->display, ":", 0);
                }
            } else {
                if (search_pattern) {
                    ssize_t result;

                    if (forward_search) {
                        result = search_forward(
                                less->current_fd,
                                search_pattern,
                                less->display->lines_offset[
                                        min(less->display->position + 1,
                                            MAX_PAD_LINE_COUNT - 1
                                        )
                                ],
                                less->ignore_case_search
                        );

                    } else {
                        int position = (int) less->display->position - 1;

                        if (position < 0) {
                            position = 0;
                        }

                        result = search_backward(
                                less->current_fd,
                                search_pattern,
                                less->display->lines_offset[(size_t) position],
                                less->ignore_case_search
                        );
                    }

                    process_search_result(less, result);
                } else {
                    redraw_menu(less->display, "No previous search", 1);
                }

            }
        } else if (strcmp(keyname(next_char), "/") == 0) {
            command_mode = 0;
            redraw_menu(less->display, "/", 0);

            if (!search_pattern) {
                search_pattern = malloc(MAX_SEARCH_PATTERN_LENGTH * sizeof(char));
            }

            if (!search_pattern) {
                die("Cannot allocate memory");
            }

            getstr(search_pattern);
            forward_search = 1;

            ssize_t result = search_forward(
                    less->current_fd,
                    search_pattern,
                    less->display->lines_offset[less->display->position],
                    less->ignore_case_search
            );

            process_search_result(less, result);
        } else if (strcmp(keyname(next_char), "?") == 0) {
            command_mode = 0;
            redraw_menu(less->display, "?", 0);

            if (!search_pattern) {
                search_pattern = malloc(MAX_SEARCH_PATTERN_LENGTH * sizeof(char));
            }

            if (!search_pattern) {
                die("Cannot allocate memory");
            }

            getstr(search_pattern);
            forward_search = 0;

            ssize_t result = search_backward(
                    less->current_fd,
                    search_pattern,
                    less->display->lines_offset[less->display->position],
                    less->ignore_case_search
            );

            process_search_result(less, result);
        } else if (strcmp(keyname(next_char), ":") == 0) {
            redraw_menu(less->display, " :", 0);
            command_mode = 1;
        } else {
            command_mode = 0;
            redraw_menu(less->display, ":", 0);
        }
    }

    free(search_pattern);
}

void switch_file(struct less *less, size_t number) {
    const int fd = open(*(less->file_names + number), O_RDONLY);

    if (fd < 0) {
        file_open_error(*(less->file_names + number));
    }

    less->current_file = number;
    less->current_fd = fd;
}

void start(struct less *less) {
    if (less->use_stdin) {
        if (isatty(STDOUT_FILENO)) {
            int result = dup2(STDIN_FILENO, 3);

            if (result < 0) {
                die("Cannot perform dup2 call");
            }

            less->current_fd = 3;

            result = dup2(STDOUT_FILENO, STDIN_FILENO);

            if (result < 0) {
                die("Cannot perform dup2 call");
            }
        } else {
            raise("Standard output in not a tty!\n");
        }
    } else {
        switch_file(less, 0);
    }

    update_buffer(less->buffer, less->current_fd);
    fill_pad(less->display, less->buffer);
    redraw_all(less->display, NULL, 1);
    main_loop(less);
}

ssize_t process_search_result(struct less *less, ssize_t result) {
    if (result > 0) {
        less->display->position = calculate_position(less, (size_t) result);
        redraw_all(less->display, ":", 0);
        return 0;
    } else if (result == INVALID_REGEXP) {
        redraw_menu(less->display, "Invalid pattern", 1);
        return -1;
    } else if (result == NO_MATCH) {
        redraw_menu(less->display, "No match found", 1);
        return -1;
    } else {
        return -1;
    }
}

size_t calculate_position(struct less *less, size_t result) {
    if (result > less->buffer->current_window_end ||
        result < less->buffer->current_window_start) {
        //Whole line with match should be in buffer
        int offset = (int) result - COLS * 4;
        less->buffer->current_window_start = offset >= 0 ? (size_t) offset : 0;

        if (result > less->buffer->limit || result < less->buffer->base) {
            update_buffer(less->buffer, less->current_fd);
        }

        fill_pad(less->display, less->buffer);
    }

    for (size_t i = 0; i < MAX_PAD_LINE_COUNT - 1; ++i) {
        if (less->display->lines_offset[i] <= result &&
            less->display->lines_offset[i + 1] > result) {
            return i;
        }
    }

    return 0;
}

// "Destructor"
void delete_less(const struct less *less) {
    endwin();
    free((void *) less->buffer);
    free((void *) less->display);
    free((void *) less);
}

void reset() {
    endwin();
}
