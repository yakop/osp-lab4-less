//
// Created by yakov on 16.11.18.
//

#ifndef LESS_COMMON_H
#define LESS_COMMON_H

#include <stdlib.h>

struct options {
    int ignore_case_search;
    int use_stdin;
    char **file_names;
    size_t file_count;
};

void file_open_error(const char *file_name);

void die(const char *message);

void raise(const char* message);

void print(const char *message);

size_t min(size_t a, size_t b);

#endif //LESS_COMMON_H
