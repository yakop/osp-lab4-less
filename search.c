//
// Created by yakov on 23.11.18.
//

#include <unistd.h>
#include <regex.h>
#include <string.h>
#include <sys/stat.h>
#include "search.h"
#include "common.h"

#define SEARCH_BUFFER_SIZE 1024 * 1024 * 16 //16 MB

static size_t update_search_buffer(int fd, char *buffer, size_t position);

ssize_t search_forward(int fd, char *text, size_t from, int ignore_case) {
    char *search_buffer = calloc(SEARCH_BUFFER_SIZE + 1, sizeof(char));

    if (!search_buffer) {
        die("Cannot allocate memory");
    }

    size_t position = from;
    update_search_buffer(fd, search_buffer, position);

    regex_t regex;
    regmatch_t regmatch;
    int flags = REG_EXTENDED;

    if (ignore_case) {
        flags |= REG_ICASE;
    }

    int result = regcomp(&regex, text, flags);

    if (result >= REG_BADPAT) {
        free(search_buffer);
        return INVALID_REGEXP;
    }

    do {
        result = regexec(&regex, search_buffer, 1, &regmatch, 0);

        if (result == 0) {
            free(search_buffer);
            return position + regmatch.rm_so;
        }

        position += SEARCH_BUFFER_SIZE;
    } while (update_search_buffer(fd, search_buffer, position) == SEARCH_BUFFER_SIZE * sizeof(char));

    free(search_buffer);
    return NO_MATCH;
}

ssize_t search_backward(int fd, char *text, size_t to, int ignore_case) {
    char *search_buffer = calloc(SEARCH_BUFFER_SIZE + 1, sizeof(char));

    if (!search_buffer) {
        die("Cannot allocate memory");
    }

    int position = (int) to - SEARCH_BUFFER_SIZE;

    if (position < 0) {
        position = 0;
    }

    update_search_buffer(fd, search_buffer, (size_t) position);

    regex_t regex;
    regmatch_t regmatch;

    int flags = REG_EXTENDED;

    if (ignore_case) {
        flags |= REG_ICASE;
    }

    int result = regcomp(&regex, text, flags);

    if (result >= REG_BADPAT) {
        free(search_buffer);
        return INVALID_REGEXP;
    }

    do {
        size_t last_match;
        size_t offset;
        result = regexec(&regex, search_buffer, 1, &regmatch, 0);

        if (result == 0 && (size_t) position + regmatch.rm_so < to) {
            last_match = (size_t) regmatch.rm_so;
            offset = (size_t) regmatch.rm_eo;

            while (regexec(&regex, search_buffer + offset, 1, &regmatch, 0) == 0 &&
                   position + offset + regmatch.rm_so < to) {
                last_match = offset + regmatch.rm_so;
                offset += regmatch.rm_eo;
            }

            free(search_buffer);
            return position + last_match;
        }

        position -= SEARCH_BUFFER_SIZE;
    } while (position >= 0 && update_search_buffer(fd, search_buffer, (size_t) position) > 0);

    free(search_buffer);
    return NO_MATCH;
}

size_t update_search_buffer(int fd, char *buffer, size_t position) {
    struct stat *stat = malloc(sizeof(struct stat));

    if (fstat(fd, stat) < 0) {
        die("Cannot stat file");
    }

    if (S_ISFIFO(stat->st_mode)) {
        return 0;
    }

    off_t offset = lseek(fd, position, SEEK_SET);

    if (offset < 0) {
        die("Cannot reset position in file");
    }

    ssize_t result = read(fd, buffer, SEARCH_BUFFER_SIZE * sizeof(char));

    if (result < 0) {
        die("Cannot read from file");
    }

    return (size_t) result;
}
