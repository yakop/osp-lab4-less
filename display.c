//
// Created by yakov on 23.11.18.
//

#include "display.h"
#include "buffer.h"
#include "less.h"

struct display *new_display(struct less *less) {
    struct display *display = calloc(1, sizeof(struct display));

    if (!display) {
        die("Cannot allocate memory");
    }

    WINDOW *pad = newpad(MAX_PAD_LINE_COUNT, COLS);
    display->itself = pad;
    display->less = less;
    return display;
}

void redraw_all(struct display *display, const char *message, int inverse) {
    prefresh(display->itself, (int) display->position, 0, 0, 0, LINES - 1, COLS);
    redraw_menu(display, message, inverse);
    refresh();
}

//Redrawing text in the bottom of the less
void redraw_menu(struct display *display, const char *message, int inverse) {
    move(LINES - 1, 0);
    clrtoeol();


    if (message) {
        if (inverse) {
            attrset(A_REVERSE);
        }

        printw(message);
        attrset(A_NORMAL);
    } else {
        attrset(A_REVERSE);

        if (display->less->use_stdin) {
            printw("standard input");
        } else {
            printw(*(display->less->file_names + display->less->current_file));

            if (display->less->file_count > 1) {
                printw(" (file %d of %d)", display->less->current_file + 1, display->less->file_count);
            }

        }

        attrset(A_NORMAL);
    }
}

void fill_pad(struct display *display, struct buffer *buffer) {
    display->position = 0;
    display->lines_offset[0] = buffer->current_window_start;
    werase(display->itself);
    wmove(display->itself, 0, 0);
    unsigned char *base = buffer->itself +
                          buffer->current_window_start -
                          buffer->base;
    size_t line_number = 1;
    size_t offset = 0;

    //Fill display with buffer contents, while at least one of them is not ended
    while (buffer->current_window_start + offset < buffer->limit
           && waddch(display->itself, *(base + offset++)) == 0) {

        if (*(base + offset - 1) == '\n') {
            display->lines_offset[line_number] = buffer->current_window_start + offset;
            line_number++;
        }
    }

    buffer->current_window_end = buffer->current_window_start + offset;
    display->line_count = (size_t) getcury(display->itself);

    //Just for beauty
    if ((int) display->line_count < LINES - 1) {
        size_t missing_count = LINES - 1 - display->line_count;

        for (size_t i = 0; i < missing_count; ++i) {
            waddstr(display->itself, "~\n");
        }
    }
}

void move_pad(struct display *display, struct buffer *buffer, enum scroll_direction direction) {
    size_t new_offset;
    size_t half_screen;
    size_t saved_position = buffer->current_window_start;
    int new_position;

    switch (direction) {
        case UP:
            half_screen = display->lines_offset[MAX_PAD_LINE_COUNT / 2] -
                          display->lines_offset[0];

            new_position = (int) display->lines_offset[0] -
                           (int) half_screen;

            if (new_position < 0) {
                new_position = 0;
            }

            buffer->current_window_start = (size_t) new_position;
            new_offset = display->lines_offset[0];

            break;
        case DOWN:
            buffer->current_window_start = display->lines_offset[MAX_PAD_LINE_COUNT / 2];
            new_offset = display->lines_offset[MAX_PAD_LINE_COUNT - 1];
    }

    if (update_buffer(buffer, display->less->current_fd) == 0) {
        fill_pad(display, buffer);

        for (size_t i = 0; i < MAX_PAD_LINE_COUNT - 1; ++i) {
            if (display->lines_offset[i] <= new_offset &&
                display->lines_offset[i + 1] > new_offset) {
                display->position = i;
            }
        }
    } else {
        buffer->current_window_start = saved_position;
    }
}

void scroll_pad(struct display *display,
                struct buffer *buffer,
                enum scroll_direction direction,
                size_t line_count) {
    switch (direction) {
        case UP:
            display->position -= min(display->position, line_count);
            redraw_all(display, ":", 0);

            //If here is the beginning of currently loaded data in display
            if (display->position == 0 && buffer->current_window_start > 0) {
                move_pad(display, buffer, UP);
                redraw_all(display, ":", 0);
            }

            break;
        case DOWN:
            display->position += line_count;
            int limit = (int) display->line_count - (LINES - 1);

            if (limit < 0) {
                limit = 0;
            }

            redraw_all(display, ":", 0);

            //End of currently loaded data in display
            if (display->position >= (size_t) limit) {
                //If it is the end of file - show end of file message
                if (buffer->current_window_end == buffer->limit && buffer->end_of_file) {
                    display->position = (size_t) limit;
                    redraw_all(display, "(END)", 1);
                } else {
                    move_pad(display, buffer, DOWN);
                    redraw_all(display, ":", 0);
                }
            }

            break;
    }
}
