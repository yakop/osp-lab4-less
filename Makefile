CC=gcc
CFLAGS1=--std=gnu99 -pedantic -Wall -Wextra -Werror -ggdb3 -L /opt/csw/lib/ -I /opt/sfw/include/
CFLAGS2=-L /opt/csw/lib/ -I /opt/sfw/include/ -lncursesw
EXECUTABLE=less
OBJECTS=main.o less.o common.o buffer.o display.o search.o

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $^ -o out/$@ $(CFLAGS2)

%.o: %.c
	$(CC) $(CFLAGS1) -c $<

clean:
	rm *.o
