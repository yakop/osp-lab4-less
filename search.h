//
// Created by yakov on 23.11.18.
//

#ifndef LESS_SEARCH_H
#define LESS_SEARCH_H

#include <stdlib.h>

#define NO_MATCH            -1
#define INVALID_REGEXP      -2

ssize_t search_forward(int fd, char* text, size_t from, int ignore_case);

ssize_t search_backward(int fd, char* text, size_t to, int ignore_case);

#endif //LESS_SEARCH_H
