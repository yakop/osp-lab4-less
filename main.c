#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "less.h"
#include "common.h"

struct options *process_argv(int argc, char **argv);

void illegal_option(char option);

void usage();

int main(int argc, char **argv) {
    struct options *opt = process_argv(argc, argv);
    struct less *less = new_less(opt);

    start(less);
    delete_less(less);

    return 0;
}

struct options *process_argv(int argc, char **argv) {

    struct options *opt = calloc(1, sizeof(struct options));

    if (!opt) {
        die("Cannot allocate memory");
    }

    if (argc < 2) {
        if (isatty(STDIN_FILENO)) {
            raise("Missing filename (\"less --help\" for help)\n");
        } else {
            opt->use_stdin = 1;
            opt->file_count = 1;
        }
    }

    for (size_t index = 1; index < (size_t) argc; ++index) {
        char *param = argv[index];

        if (param[0] == '-') {
            switch (param[1]) {
                case 'i':
                    opt->ignore_case_search = 1;
                    break;
                case '?' :
                    usage();
                    return NULL;
                case 0:
                    opt->use_stdin = 1;
                    opt->file_count = 1;
                    break;
                default :
                    illegal_option(param[1]);
                    return NULL;
            }
        } else {
            opt->file_count = argc - index;
            opt->file_names = argv + index;
            break;
        }
    }

    return opt;
}

void usage() {
    print("Usage: less [-|?|i] [filename ...]\n");
}

void illegal_option(char option) {
    const char *no_option_template = "There is no -%c option (\"less --help\" for help)\n";
    char *message = malloc(strlen(no_option_template));

    if (!message) {
        die("Cannot allocate memory");
    }

    sprintf(message, no_option_template, option);
    print(message);
    free(message);
}
