//
// Created by yakov on 16.11.18.
//

#ifndef LESS_LESS_H
#define LESS_LESS_H

#include "common.h"
#include <stdlib.h>

struct less {
    int ignore_case_search;
    int use_stdin;
    size_t file_count;
    size_t current_file;
    int current_fd;
    char** file_names;
    struct display* display;
    struct buffer* buffer;
};

struct less *new_less(const struct options *options);

void start(struct less *less);

void delete_less(const struct less *less);

#endif //LESS_LESS_H
