//
// Created by yakov on 23.11.18.
//

#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include "buffer.h"
#include "common.h"
#include "less.h"

struct buffer *new_buffer() {
    struct buffer *buffer = calloc(1, sizeof(struct buffer));

    if (!buffer) {
        die("Cannot allocate memory");
    }

    buffer->itself = calloc(LESS_BUFFER_SIZE, sizeof(char));

    if (!buffer->itself) {
        die("Cannot allocate memory");
    }

    return buffer;
}

int update_buffer(struct buffer *buffer, int fd) {
    struct stat *stat = malloc(sizeof(struct stat));

    if (!stat) {
        die("Cannot allocate memory");
    }

    if (fstat(fd, stat) < 0) {
        die("Cannot stat file");
    }

    if (S_ISFIFO(stat->st_mode)) {
        if (buffer->current_window_start >= buffer->base
            && buffer->current_window_start <= buffer->limit) {
            memmove(buffer->itself,
                    buffer->itself + buffer->current_window_start - buffer->base,
                    buffer->limit - buffer->current_window_start);

            buffer->base = buffer->current_window_start;

            ssize_t result = read(fd,
                                  buffer->itself + buffer->limit - buffer->current_window_start,
                                  LESS_BUFFER_SIZE - buffer->current_window_start - buffer->base);

            if (result < 0) {
                die("Cannot read from file");
            }

            buffer->limit = buffer->base + result;
        } else {
            free(stat);
            return 1;
        }
    } else {
        off_t offset = lseek(fd, buffer->current_window_start, SEEK_SET);

        if (offset < 0) {
            die("Cannot reset position in file");
        }

        buffer->base = (size_t) offset;
        ssize_t result = read(fd, buffer->itself, LESS_BUFFER_SIZE);

        if (result < 0) {
            die("Cannot read from file");
        }

        buffer->end_of_file = result < LESS_BUFFER_SIZE;
        buffer->limit = buffer->base + result;
    }

    free(stat);
    return 0;
}
