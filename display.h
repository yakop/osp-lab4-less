//
// Created by yakov on 23.11.18.
//

#ifndef LESS_DISPLAY_H
#define LESS_DISPLAY_H

#include <stdlib.h>
#include "buffer.h"
#if defined(__FreeBSD__) || defined(__linux__)

#include <ncurses.h>

#else
#include <ncurses/ncurses.h>
#endif /* __FreeBSD__ || __linux__ */

#define MAX_PAD_LINE_COUNT 1024 * 8 //8 K, because of pads's limitations

//TODO: implement cool scrolling
struct display {
    size_t line_count;
    size_t position;
    size_t lines_offset[MAX_PAD_LINE_COUNT];
    WINDOW *itself;
    struct less *less;
};

enum scroll_direction {
    UP,
    DOWN
};

struct display *new_display(struct less *less);

void redraw_all(struct display *display, const char *message, int inverse);

void redraw_menu(struct display *display, const char *message, int inverse);

void scroll_pad(struct display *display,
                struct buffer *buffer,
                enum scroll_direction direction,
                size_t line_count);

void fill_pad(struct display *display, struct buffer *buffer);

void move_pad(struct display *display, struct buffer *buffer, enum scroll_direction direction);

#endif //LESS_DISPLAY_H
